class Tracker < ApplicationRecord
  has_many :tracker_hits, dependent: :destroy

  before_create do
    self.token = SecureRandom.uuid
  end

  def touch(ip: nil)
    self.tracker_hits.create(ip: ip)
  end
end
