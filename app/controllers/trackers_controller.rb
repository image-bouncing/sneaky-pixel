Mime::Type.register 'image/jpg', :jpg
Mime::Type.register 'image/png', :png
Mime::Type.register 'image/webp', :webp

class TrackersController < ApplicationController
  EMPTY_PNG = Base64.decode64('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=')

  def index
    @trackers = Tracker.all
  end

  def show
    @tracker = Tracker.includes(:tracker_hits).find_by(token: params[:token])

    respond_to do |format|
      format.html do
        @base_url = request.base_url
      end
      format.any(:jpg, :png, :webp) do
        @tracker.touch(ip: request.ip)
        render plain: nil
      end
    end
  end

  def create
    @tracker = Tracker.create

    redirect_to tracker_url(@tracker.token)
  end
end
