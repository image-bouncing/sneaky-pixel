require 'test_helper'

class TrackerTest < ActiveSupport::TestCase
  test 'tracker is initialized with uuid' do
    tracker = Tracker.create
    assert tracker.token, 'tracker created without uuid'
  end

  test '#touch creates a tracker hit' do
    tracker = trackers(:new)
    assert_difference 'tracker.tracker_hits.count' do
      tracker.touch
    end
  end

  test '#touch with an ip parameter creates a tracker hit with the ip' do
    tracker = trackers(:new)
    assert_difference 'tracker.tracker_hits.count' do
      address = IPAddr.new '3ffe:505:2::1'
      tracker.touch(ip: address)
    end
  end
end
