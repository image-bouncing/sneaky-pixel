require 'application_system_test_case'

class TrackersControllerTest < ApplicationSystemTestCase
  test 'visiting the tracker index' do
    visit trackers_url
    assert_selector 'h1', text: 'Trackers'
    assert_selector 'li', count: trackers.count
  end

  test 'visiting a tracker shows list of hits and embed code' do
    tracker = trackers(:popular)
    assert_no_difference 'tracker.tracker_hits.count' do
      visit tracker_url(token: tracker.token)
      assert_selector 'h1', text: 'Tracker'
      assert_selector 'li', count: 3
      assert_selector 'textarea', text: '<img src='
    end
  end

  test 'hitting a tracker file' do
    tracker = trackers(:new)
    assert_difference 'tracker.tracker_hits.count', 3 do
      visit tracker_url(token: tracker.token, format: :jpg)
      visit tracker_url(token: tracker.token, format: :png)
      visit tracker_url(token: tracker.token, format: :webp)
    end
  end
end
