class CreateTrackers < ActiveRecord::Migration[6.0]
  def change
    create_table :trackers do |t|
      t.uuid :token

      t.timestamps
    end
  end
end
