class CreateTrackerHits < ActiveRecord::Migration[6.0]
  def change
    create_table :tracker_hits do |t|
      t.belongs_to :tracker, null: false, foreign_key: true
      t.inet :ip

      t.timestamps
    end
  end
end
