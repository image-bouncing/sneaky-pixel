Rails.application.routes.draw do
  root 'trackers#index'
  resources :trackers, only: [:index, :show, :create], param: :token
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
