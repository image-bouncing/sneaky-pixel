# Welcome to Sneaky Pixel

## What's Sneaky Pixel?

Sneaky Pixel is a helper app to create _view-tracking image tags_. Commonly these are used by email management systems (Convertkit, MailChimp, Mailerlite, etc) to track whether recipients have opened emails.

## Usage

This application isn't actually intended to be used for creating image tracking tags for real usage (hence the barebones UI and lack of complex integration support) - this is intended to aide in testing methods to _prevent_ the usage of these tags.

## How Does View Tracking Work?

For each email sent, each recipient will get a unique image tag pointing back to the provider's server. The server-side image is typically a 1px by 1px transparent image so it doesn't render on the client when the email is viewed (sneaky, right?).

The server listens for download requests for the image at that URL and when a download request is received, a view is registered. This could be via a boolean (for simple open tracking) or via a counter.

## Why Should I Care?

The web and privacy have a convoluted relationship. Over the years, it's become standard practice for companies to harvest every piece of information they can about you.

View tracking is one such method. Email list owners utilize view tracking to customize marketing campaigns for customer segments and to send campaigns to re-engage or upsell customers.

Identifying ways to mitigate view tracking helps build a more secure, more private web experience.

# Using Sneaky Pixel

For quick access, see the demo page at https://sneaky-pixel.chrisdurheim.com

## Set Up

```
# Get the repo

$ git clone git@gitlab.com:image-bouncing/sneaky-pixel.git

# change directory and install the gems for the app

$ cd sneaky-pixel
$ bundle install

# set up the database for the app

$ bundle exec rails db:create
$ bundle exec rails db:migrate

# start the app server

$ bundle exec rails s
```

Navigate to [localhost:3000/trackers](http://localhost:3000/trackers) to see a list of trackers.

## Creating and Using a Tracker

1. On the `Trackers` page, click `Create new` to create a new tracker.
2. This will take you to the tracker page which shows you the HTML embed code for the tracker as well as a list of hits (views).
3. Paste the HTML embed code into an HTML email and send to your test email account.
4. Note that the tracker hit list is empty even after sending
5. Open the email in the test email account
6. Note that the tracker hit list now registers a hit based on the view

The `hit` list will show the IP address and relative timestamp from each time the image was viewed.

The tracker could also be used on a webpage for view tracking.

Note: CDNs and client-side caching may obfuscate the actual view/hit count.

## How Sneaky Pixel Works

Sneaky pixel utilizes Ruby on Rails routing to route the asset request for a given `.jpg` file through a controller that increments a tracking counter in the database. It then renders an empty image to the client to prevent interference/detection by a user (unless they are inspecting the code).
